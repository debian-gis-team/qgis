#!/bin/sh

if dpkg -s qgis-plugin-grass >/dev/null 2>&1; then
	if [ "$LD_LIBRARY_PATH" = "" ]; then
		LD_LIBRARY_PATH=/usr/lib/grass84/lib
	else
		LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/grass84/lib
	fi
fi

export LD_LIBRARY_PATH

if [ -r /etc/default/qgis ]; then
	. /etc/default/qgis
fi

if [ "$(basename "$0")" = "qgis_process" ]; then
	QGIS_OPTS="$(echo "${QGIS_OPTS}" | sed 's/--noversioncheck//')"
fi

exec $0.bin $QGIS_OPTS "$@"
