Source: qgis
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>,
           Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: bison,
               ca-certificates,
               cmake,
               debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               flex,
               gdal-bin,
               grass-dev (>= 8.4.1),
               libexiv2-dev,
               libexpat1-dev,
               libfcgi-dev,
               libgdal-dev (>= 1.11),
               libgeos-dev (>= 3.0.0),
               libgsl-dev,
               libpq-dev,
               libproj-dev,
               libprotobuf-dev,
               libqca-qt5-2-dev,
               libqca-qt5-2-plugins,
               libqscintilla2-qt5-dev,
               libqt5opengl5-dev,
               libqt5serialport5-dev,
               libqt5sql5-sqlite,
               libqt5svg5-dev,
               libqt5xmlpatterns5-dev,
               libqwt-qt5-dev,
               libspatialindex-dev,
               libspatialite-dev,
               libsqlite3-dev,
               libsqlite3-mod-spatialite,
               libzip-dev,
               libzstd-dev,
               ninja-build,
               nlohmann-json3-dev,
               ocl-icd-opencl-dev,
               opencl-headers,
               pkgconf,
               protobuf-compiler,
               pyqt5-dev-tools,
               pyqt5-dev,
               pyqt5.qsci-dev,
               python3-dev,
               python3-gdal,
               python3-nose2,
               python3-owslib,
               python3-psycopg2,
               python3-pyqt5,
               python3-pyqt5.qsci,
               python3-pyqt5.qtpositioning,
               python3-pyqt5.qtserialport,
               python3-pyqt5.qtsql,
               python3-pyqt5.qtsvg,
               python3-pyqtbuild,
               python3-sipbuild,
               python3-termcolor,
               python3-yaml,
               qt3d5-dev,
               qt3d-assimpsceneimport-plugin,
               qt3d-defaultgeometryloader-plugin,
               qt3d-gltfsceneio-plugin,
               qt3d-scene2d-plugin,
               qt5keychain-dev,
               qtbase5-dev,
               qtbase5-private-dev,
               qtkeychain-qt5-dev,
               qtmultimedia5-dev,
               qtpositioning5-dev,
               qttools5-dev-tools,
               qttools5-dev,
               sip-tools,
               git,
               doxygen,
               graphviz,
               xvfb,
               xauth,
               xfonts-base,
               xfonts-100dpi,
               xfonts-75dpi,
               xfonts-scalable,
               spawn-fcgi,
               poppler-utils,
               locales
Build-Conflicts: libqgis-dev,
                 qgis-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/qgis
Vcs-Git: https://salsa.debian.org/debian-gis-team/qgis.git
Homepage: https://qgis.org/
Rules-Requires-Root: no

Package: qgis
Architecture: any
Depends: python3-qgis (= ${binary:Version}),
         qgis-common (= ${source:Version}),
         qgis-providers (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: qgis-plugin-grass
Suggests: gpsbabel
Breaks: qgis-common (<< 3.4.5),
        qgis-provider-grass (<< 2.14.0)
Conflicts: uim-qt3
Replaces: qgis-common (<< 3.4.5),
          qgis-provider-grass (<< 2.14.0)
Description: Geographic Information System (GIS)
 A Geographic Information System (GIS) manages, analyzes, and displays
 databases of geographic information. QGIS supports shape file viewing and
 editing, spatial data storage with PostgreSQL/PostGIS, projection on-the-fly,
 map composition, and a number of other features via a plugin interface. QGIS
 also supports display of various georeferenced raster and Digital Elevation
 Model (DEM) formats including GeoTIFF, Arc/Info ASCII Grid, and USGS ASCII
 DEM.

Package: qgis-common
Architecture: all
Depends: libjs-jquery,
         libjs-leaflet,
         qml-module-qtcharts,
         ${misc:Depends}
Breaks: qgis (<< 2.18.17)
Replaces: qgis (<< 2.18.17)
Description: QGIS - architecture-independent data
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains architecture-independent supporting data files for use
 with QGIS.

Package: libqgis-3d3.40.4
Architecture: any
Section: libs
Depends: qt3d-assimpsceneimport-plugin,
         qt3d-defaultgeometryloader-plugin,
         qt3d-gltfsceneio-plugin,
         qt3d-scene2d-plugin,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared 3d library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared 3d library.

Package: libqgis-analysis3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared analysis library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared analysis library.

Package: libqgis-app3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared app library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared app library.

Package: libqgis-core3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared core library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared core library.

Package: libqgis-gui3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared gui library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared gui library.

Package: libqgis-native3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared native gui library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared native gui library.

Package: libqgis-server3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared server library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared server library.

Package: libqgisgrass8-3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared grass library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared grass library.

Package: libqgispython3.40.4
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS - shared Python library
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the shared Python library.

Package: libqgis-customwidgets
Architecture: any
Depends: qttools5-dev-tools,
         ${shlibs:Depends},
         ${misc:Depends}
Description: QGIS custom widgets for Qt Designer
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains a library to use specific QGIS widgets in Qt Designer.

Package: libqgis-dev
Architecture: any
Section: libdevel
Depends: grass-dev (>= 8.4.0),
         libexiv2-dev,
         libexpat1-dev,
         libgdal-dev (>= 1.11),
         libgeos-dev (>= 3.0.0),
         libgsl-dev,
         libpq-dev,
         libproj-dev,
         libqca-qt5-2-dev,
         libqgis-3d3.40.4 (= ${binary:Version}),
         libqgis-analysis3.40.4 (= ${binary:Version}),
         libqgis-app3.40.4 (= ${binary:Version}),
         libqgis-core3.40.4 (= ${binary:Version}),
         libqgis-gui3.40.4 (= ${binary:Version}),
         libqgis-native3.40.4 (= ${binary:Version}),
         libqgis-server3.40.4 (= ${binary:Version}),
         libqgisgrass8-3.40.4 (= ${binary:Version}),
         libqgispython3.40.4 (= ${binary:Version}),
         libqscintilla2-qt5-dev,
         libqt5svg5-dev,
         libqwt-qt5-dev,
         libsqlite3-dev,
         nlohmann-json3-dev,
         pyqt5-dev-tools,
         pyqt5.qsci-dev,
         python3-dev,
         python3-pyqt5,
         python3-pyqt5.qsci,
         python3-pyqt5.qtmultimedia,
         python3-pyqt5.qtsql,
         python3-pyqt5.sip,
         python3-pyqtbuild,
         qgis-sip (= ${source:Version}),
         qtbase5-dev,
         qtpositioning5-dev,
         qttools5-dev,
         sip-tools,
         ${misc:Depends}
Recommends: libqgis-customwidgets
Provides: qgis-dev
Description: QGIS - development files
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the headers and libraries needed to develop plugins for
 QGIS.

Package: qgis-sip
Architecture: all
Section: devel
Depends: ${misc:Depends}
Description: SIP specification files for QGIS
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the SIP specification files.

Package: qgis-provider-grass
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: GRASS provider for QGIS
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This provider enables GRASS data access in QGIS.

Package: qgis-plugin-grass
Architecture: any
Depends: qgis (= ${binary:Version}),
         qgis-plugin-grass-common (= ${source:Version}),
         qgis-provider-grass (= ${binary:Version}),
         grass-core (>= 8.4.0),
         ${grass:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-provider-grass (<< 2.11.0+git20151002)
Replaces: qgis-provider-grass (<< 2.11.0+git20151002)
Description: GRASS plugin for QGIS
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This plugin enables GRASS data access in QGIS.

Package: qgis-plugin-grass-common
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Breaks: qgis-common (<< 1.5)
Replaces: qgis-common (<< 1.5)
Description: GRASS plugin for QGIS - architecture-independent data
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains architecture-independent supporting data files for use
 with the QGIS GRASS plugin.

Package: python3-qgis
Architecture: any
Section: python
Depends: python3-qgis-common (= ${source:Version}),
         qgis-providers (= ${binary:Version}),
         libqgispython3.40.4,
         libsqlite3-mod-spatialite,
         python3-jinja2,
         python3-lxml,
         python3-owslib,
         python3-plotly,
         python3-psycopg2,
         python3-pygments,
         python3-pyproj,
         python3-pyqt5,
         python3-pyqt5.qsci,
         python3-pyqt5.qtmultimedia,
         python3-pyqt5.qtpositioning,
         python3-pyqt5.qtserialport,
         python3-pyqt5.qtsql,
         python3-pyqt5.qtsvg,
         python3-yaml,
         ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: Python bindings to QGIS
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the files for the Python support.

Package: python3-qgis-common
Architecture: all
Section: python
Depends: gdal-bin,
         python3-gdal,
         python3-matplotlib,
         libqgis-customwidgets (>= ${source:Version}),
         ${python3:Depends},
         ${misc:Depends}
Breaks: python-qgis-common (<< 3.4.5)
Replaces: python-qgis-common (<< 3.4.5)
Description: Python bindings to QGIS - architecture-independent files
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains architecture-independent files for the QGIS Python
 bindings.

Package: qgis-providers
Architecture: any
Pre-Depends: dpkg (>= 1.16.1),
             ${misc:Pre-Depends}
Depends: qgis-providers-common (= ${source:Version}),
         libqca-qt5-2-plugins,
         libqt5sql5-sqlite,
         qt5-image-formats-plugins,
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: qt5-image-formats-plugin-pdf
Breaks: qgis (<= 1.6)
Replaces: qgis (<= 1.6)
Description: collection of data providers to QGIS
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the provider plugins for QGIS.

Package: qgis-providers-common
Architecture: all
Depends: ${misc:Depends}
Breaks: qgis-common (<< 3.4.11)
Replaces: qgis-common (<< 3.4.11)
Description: collection of data providers to QGIS - architecture-independent files
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains architecture-independent files for the QGIS providers.

Package: qgis-server
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         qgis-server-landingpage (= ${binary:Version}),
         qgis-server-wcs (= ${binary:Version}),
         qgis-server-wfs (= ${binary:Version}),
         qgis-server-wfs3 (= ${binary:Version}),
         qgis-server-wms (= ${binary:Version}),
         qgis-server-wmts (= ${binary:Version}),
         ${misc:Depends}
Description: QGIS server providing various OGC services
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This metapackage depends on the individual QGIS server components.

Package: qgis-server-common
Architecture: all
Depends: ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (data)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the QGIS server data.

Package: qgis-server-bin
Architecture: any
Depends: qgis-server-common (= ${source:Version}),
         python3-qgis (= ${binary:Version}),
         qgis-providers (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (executables)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the QGIS server executables.

Package: qgis-server-landingpage
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (landing page)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the landing page service.

Package: qgis-server-wcs
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (WCS)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the WCS service.

Package: qgis-server-wfs
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (WFS)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the WFS service.

Package: qgis-server-wfs3
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (WFS 3)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the WFS 3 service.

Package: qgis-server-wms
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (WMS)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the WMS service.

Package: qgis-server-wmts
Architecture: any
Depends: qgis-server-bin (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Breaks: qgis-server (<< 3.16)
Replaces: qgis-server (<< 3.16)
Description: QGIS server providing various OGC services (WMTS)
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the WMTS service.

Package: qgis-api-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: qt5-doc-html
Description: QGIS API documentation
 QGIS is a Geographic Information System (GIS) which manages, analyzes and
 display databases of geographic information.
 .
 This package contains the QGIS API documentation.
